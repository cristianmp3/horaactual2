<%-- 
    Document   : hora actual
    Created on : 03-abr-2020, 14:30:06
    Author     : Cristian Martinez Sanhueza, Taller de app empresariales CIISA
    Abril año 2020
--%>

<%@page import="java.time.LocalTime"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <%! LocalTime hora; %> 
    <%
    hora = LocalTime.now() ;
    %>
    <body> 
        <h1>La hora actual es: <%=hora %> </h1>
    </body>
</html>
